# БАШНЯ. Курс по фронту. Код к занятию 7

### На занятии мы разбирали
* Формат GeoJSON
* Работу с библиотекой Mapbox GL

### Полезные ссылки
* [Про GeoJSON в википедии](https://en.wikipedia.org/wiki/GeoJSON)
* [Статейка с хабра, раскрывающая формат GeoJSON](https://habr.com/ru/companies/ruvds/articles/489828/)
* [Mapbox GL JS можно найти тут](https://www.mapbox.com/mapbox-gljs)
* [Крутые примеры работы с Mapbox GL JS](https://docs.mapbox.com/mapbox-gl-js/example/)
* [Сравнение картографических сервисов](https://techrocks.ru/2019/10/05/best-javascript-maps-api-and-libraries/)
