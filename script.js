import districtData from './data/districts.geojson' assert { type: 'json' };
import governmentData from './data/governments.geojson' assert { type: 'json' };

// Создаем карту
mapboxgl.accessToken = 'pk.eyJ1IjoidXN1YWwtb25lIiwiYSI6ImNrdXIzaWd2bjA5dG4yd3A1dW01NThpdnUifQ.ylUj7pDzpb1yx0d321uzyg';
const map = new mapboxgl.Map({
  container: 'map',
  style: 'mapbox://styles/mapbox/streets-v12',
  center: [37.618423, 55.751244],
  zoom: 9, 
});

map.on('load', () => {
  // Наполняем селект районами из датасета
  const select = document.getElementById('districtSelect');
  const districtNames = districtData.features.map(feature => feature.properties.NAME).sort();
  for (const name of districtNames) {
    const optionElement = document.createElement('option');
    optionElement.setAttribute('value', name);
    optionElement.innerHTML = name;
    select.appendChild(optionElement);
  }

  // Достаем все, что нам нужно из датасета с управами
  const districtGovernments = governmentData.features.map(feature => ({
    geometry: feature.geometry,
    district: feature.properties.Attributes.District.replace('поселение', '').replace('район', '').trim(),
  }));

  // Маркер, который будет указывать, где находится управа
  const marker = new mapboxgl.Marker();



  select.addEventListener('change', event => {
    // Очищаем предыдущий отображенный район
    if (map.getLayer('district-fill')) {
      map.removeLayer('district-fill');
    }
    if (map.getLayer('district-outline')) {
      map.removeLayer('district-outline');
    }
    if (map.getSource('district')) {
      map.removeSource('district');
    }
    // Удаляем маркер с карты
    marker.remove();

    const name = event.target.value;
    if (!name.length) { return; }

    const district = districtData.features.find(feature => feature.properties.NAME === name);
    const government = districtGovernments.find(g => g.district === name);

    // Отрисовываем район
    map.addSource('district', {
      type: 'geojson',
      data: district,
    });

    map.addLayer({
      id: 'district-fill',
      type: 'fill',
      source: 'district',
      paint: {
        'fill-color': '#0080ff',
        'fill-opacity': 0.2
      }
    });

    map.addLayer({
      id: 'district-outline',
      type: 'line',
      source: 'district',
      paint: {
        'line-color': '#0080ff',
        'line-width': 2
      }
    });

    // Ставим маркер на карту
    marker.setLngLat(government.geometry.coordinates).addTo(map);

    // Перемещаем карту к отрисованному району
    const nw = [
      Math.min(...district.geometry.coordinates[0].map(coords => coords[0])),
      Math.max(...district.geometry.coordinates[0].map(coords => coords[1])),
    ];
    const se = [
      Math.max(...district.geometry.coordinates[0].map(coords => coords[0])),
      Math.min(...district.geometry.coordinates[0].map(coords => coords[1])),
    ];
    const bounds = new mapboxgl.LngLatBounds(nw, se);
    map.fitBounds(bounds, { padding: 20 });
  });
});
